﻿// ConsoleApplication1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <iostream>
using namespace std;

struct TNode {
    int Data;
    TNode* pNext;
};
void create(TNode** pBegin, TNode** pEnd, int Data) {
    *pBegin = new TNode;
    (*pBegin)->Data = Data;
    (*pBegin)->pNext = NULL;
    *pEnd = *pBegin;
}

void add(TNode** pEnd, int Data) {
    TNode* pv = new TNode;
    pv->Data = Data;
    pv->pNext = NULL;
    (*pEnd)->pNext = pv;
    *pEnd = pv;
}

int del(TNode** pBegin, TNode** pEnd, int Data) {
    TNode* pPrev = NULL;
    TNode* pCurrent = *pBegin;
    *pBegin = (*pBegin)->pNext;
    int value = pCurrent->Data;
    delete pCurrent;
    return value;
}

int main()
{
    TNode* pBegin = NULL;
    TNode* pEnd = NULL;
    int Data = 0, Chislo = 0;
    float res = 1, sred=0;
    cout << "Vvedite Kol-vo elementov ";
    cin >> Chislo;
    create(&pBegin, &pEnd, Chislo);
    cout << "Vvedite chislo ";
    cin >> Data;
    res = res * Data;
    sred = pow(res,  (1.0/ Chislo));
    for (int i = 1; i < Chislo; i++)
    {
        add(&pEnd, Chislo);
        cout << "Vveite chislo ";
        cin >> Data;
        res = res * Data;
        sred = pow(res, 1.0/Chislo);
    }
    cout << "\nSrednee geometricheskoe ";
    cout << sred;
    for (int i = 0; i < Chislo; i++) {
        del(&pBegin, &pEnd, Chislo);
    }
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
